#!/bin/bash -e
CONTAINER="${USER}_panther_vm"
START_SHELL="sh"
IMAGE="blackpanther/latest:latest"
WORKSPACE=${WORKSPACE:-$PWD}


echo $WORKSPACE
# test if the container is running
HASH=`docker ps -q -f name=$CONTAINER`
 # test if the container is stopped
HASH_STOPPED=`docker ps -qa -f name=$CONTAINER`


if [ -n "$HASH" ];then
    echo "founding existing running container $CONTAINER, proceeed to exec another shell"
    docker exec -it $HASH $START_SHELL


elif [ -n "$HASH_STOPPED" ];then
    echo "founding existing stopped container $CONTAINER, proceeed to start"
    docker start --attach -i $HASH_STOPPED


else
    docker run \
        --rm -it -p 5555:5555 \
        --name=$CONTAINER \
        --hostname=$CONTAINER \
        -v "$WORKSPACE:/BlackPanther" \
        -w "/BlackPanther" \
        -v ~/.ssh:/root/.ssh \
        -v $HOME/.gitconfig:/root/.gitconfig \
        --entrypoint $START_SHELL $IMAGE \
        # -xec 'python3 ./src/facial_detect.py'
fi


echo "see you, use 'docker rm $CONTAINER' to kill the vm if you want a fresh env next time"
