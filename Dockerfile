# main image to operate 
FROM yoanlin/opencv-python3
RUN apt-get update
RUN apt-get install -y software-properties-common
RUN add-apt-repository "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) universe"
RUN apt-get install -y supervisor
ADD . /BlackPanther
WORKDIR /BlackPanther
RUN pip3 install -r requirements.txt
EXPOSE 5555
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
CMD ["/usr/bin/supervisord"]
