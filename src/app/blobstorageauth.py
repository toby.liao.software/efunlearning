from datetime import datetime, timedelta
import requests
from azure.storage.blob import (
    BlockBlobService,
    ContainerPermissions
)
import config


class BlobSASToken(object):
    def __init__(self):
        self.config = config.global_config
        self.accountName = self.config.ACCT_NAME
        self.accountKey = self.config.ACCT_KEY
        self.containerName = self.config.CONT_NAME
    
    def get_token(self):
        blobService = BlockBlobService(
            account_name = self.accountName,
            account_key = self.accountKey)
        sas_token = blobService.generate_container_shared_access_signature(
            self.containerName,
            ContainerPermissions.READ,
            datetime.utcnow() + timedelta(hours=1))
        return sas_token
