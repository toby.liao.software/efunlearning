from zmq_server import ZMQServer
import config

config = config.global_config
zs = ZMQServer(config.SUB_KEY, config.API_URL, config.IMG_URL)
zs.activate_server()
