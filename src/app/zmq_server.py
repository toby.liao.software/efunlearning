import time
import numpy as np
import zmq
import base64
import logging
# integrate
from blobstorageauth import BlobSASToken
from eyetracking import EyeTracking
from emotion import EmotionDetection
from label_generator import Labeler

PROTOCOL = 'tcp'
PORT_NUM = '5555'
FORMAT = '%(asctime)-15s %(port)s %(protocol)-8s %(message)s'
logging.basicConfig(format=FORMAT)
d = {'port': PORT_NUM, 'protocol': PROTOCOL}
logger = logging.getLogger('ZMQServer')
logger.setLevel(logging.INFO)

class SerializingSocket(zmq.Socket):
    def recv_array(self, flags=0, copy=True, track=False):
        # receiving the json format of the file
        md = self.recv_json(flags=flags)
        try:
            auth = BlobSASToken()
            token = auth.get_token()
        except:
            logger.error('ZMQ Server: %s', 'Authentication Error', extra=d)
        try:
            md['arrayname'] = base64.b64decode(md['arrayname']).decode("utf-8")
            md['url'] = base64.b64decode(md['url']).decode("utf-8")
            md['url'] = "{}?{}".format(md['url'], token)
            msg = self.recv(flags=flags, copy=copy, track=track)
            return (md['arrayname'], md['url'])
        
        except:
            logger.error('ZMQ Server: %s', 'Received packet error', extra=d)
        

class SerializingContext(zmq.Context):
    _socket_class = SerializingSocket

class ZMQServer(object):
    def __init__(self, sub_key, api_url, img_url):
        logger.info('ZMQ Server: %s', 'Initialized', extra=d)
        self.context = SerializingContext()
        self.socket = self.context.socket(zmq.REP)
        self.socket.bind("{}://*:{}".format(PROTOCOL, PORT_NUM))
        self.sub_key = sub_key
        self.api_url = api_url
        self.img_url = img_url
        self.pkt2gen = {}
        self.label = Labeler()
    
    def activate_server(self):
        while True:
            arrayname, img_url = self.socket.recv_array(copy=False)
            logger.info('ZMQ Server: %s', 'Received packet from client', extra=d)
            self.img_url = img_url
            #  Send reply back to client
            self.socket.send(b"Server well received.")
            logger.info('ZMQ Server: %s', 'Return acknowledgement to client', extra=d)
            et = EyeTracking(self.img_url)
            ed = EmotionDetection(self.sub_key, self.api_url, self.img_url)
            if ed.get_result():
                _emo, _occu, _hp = ed.get_result()
                _et = et.get_result()
                logger.info('EyeTracking: %s', _et, extra=d)
                self.pkt2gen['et'] = _et
                logger.info('Emotion: %s', _emo, extra=d)
                self.pkt2gen['emo'] = _emo
                logger.info('Occlusion: %s', _occu, extra=d)
                self.pkt2gen['occu'] = _occu
                logger.info('Head Pose: %s', _hp, extra=d)
                self.pkt2gen['hp'] = _hp
                self.label.get_outcome(self.pkt2gen, arrayname)
            else:
                logger.info('Human Detect Server: %s', 'null', extra=d)
    
    def __repr__(self):
        return str(self.json)

    def __getattr__(self, attr):
        if attr in self.json:
            return self.json[attr]
        return None
    
