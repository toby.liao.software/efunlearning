import cv2
import numpy as np
from gaze_tracking import GazeTracking
from urllib.request import urlopen
import ssl

ssl._create_default_https_context = ssl._create_unverified_context


class EyeTracking(object):
    def __init__(self, img_url):
        self.eyeTrackingOutcome = ""
        self.img = self.url2image(img_url)
        self.gaze = GazeTracking()

    @staticmethod
    def url2image(img_url):
        resp = urlopen(img_url)
        image = np.asarray(bytearray(resp.read()), dtype="uint8")
        image = cv2.imdecode(image, cv2.IMREAD_COLOR)
        return image

    def detection_criteria(self):
        self.gaze.refresh(self.img)
        if self.gaze.is_blinking():
            self.eyeTrackingOutcome = "blink"
        elif self.gaze.is_right():
            self.eyeTrackingOutcome = "left"
        elif self.gaze.is_left():
            self.eyeTrackingOutcome = "right"
        elif self.gaze.is_center():
            self.eyeTrackingOutcome = "center"
    
    def get_result(self):
        self.detection_criteria()
        return self.eyeTrackingOutcome
