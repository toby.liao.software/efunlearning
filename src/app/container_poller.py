import cv2
import numpy as np
import zmq
from urllib.request import urlopen
import ssl
import time
import base64
import logging
from azure.storage.blob import BlockBlobService, PublicAccess
import config

PROTOCOL = 'tcp'
PORT_NUM = '5555'
FORMAT = '%(asctime)-15s %(port)s %(protocol)-8s %(message)s'
logging.basicConfig(format=FORMAT)
d = {'port': PORT_NUM, 'protocol': PROTOCOL}
logger = logging.getLogger('ContentPoller')
logger.setLevel(logging.INFO)

config = config.global_config
ssl._create_default_https_context = ssl._create_unverified_context
block_blob_service = BlockBlobService(account_name=config.ACCT_NAME, account_key=config.ACCT_KEY)
dir_status = []

class SerializingSocket(zmq.Socket):
    def send_array(self, A, arrayname="NoName",flags=0, copy=True, track=False):
        """send a numpy array with metadata and array name"""
        md = dict(
            arrayname = arrayname.decode("utf-8"),
            url = A.decode("utf-8"),
            md = "To be added"
        )
        self.send_json(md, flags|zmq.SNDMORE)
        return self.send(bytes([flags]), copy=copy, track=track)

class SerializingContext(zmq.Context):
    _socket_class = SerializingSocket

class ZMQClient(object):
    def __init__(self, url):
        self.url = url
        self.message = ""
        self.context = SerializingContext()
        logger.info('Poller: %s', 'Connecting to ZMQ Server...', extra=d)
        self.socket = self.context.socket(zmq.REQ)
        self.socket.connect("{}://localhost:{}".format(PROTOCOL, PORT_NUM))
        self.arrayname = 'default'
    
    
    def packet_operation(self, file_name):
        logger.info('Poller: %s', 'ZMQ Server Connection Established.', extra=d)
        self.arrayname = file_name
        self.socket.send_array(
            base64.encodebytes(self.url.encode()),
            base64.encodebytes(self.arrayname.encode()),
            copy=False)
        self.message = self.socket.recv()
        logger.info('Poller: Received response [%s]', self.message, extra=d)
        

if __name__ == "__main__":
    logger.info('Poller: %s', 'Scanning blobs in the container(Every 5 seconds).', extra=d)
    while True:
        try:
            generator = block_blob_service.list_blobs(config.CONT_NAME)
            for element in generator:
                if element.name not in dir_status:
                    url = "https://{}.blob.core.windows.net/{}/{}".format(config.ACCT_NAME, config.CONT_NAME, element.name)
                    dir_status.append(element.name)
                    zc = ZMQClient(url)
                    zc.packet_operation(element.name)
            
        except Exception as e:
            logger.error('Poller: %s', e, extra=d)
        time.sleep(5)
