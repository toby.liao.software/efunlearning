#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import os
import simplejson as json
from future.utils import iteritems

logger = logging.getLogger(__name__)


class Configuration(object):
    def __init__(self):
        self.env = "staging"
        self.json = None

        if "ENV" in os.environ:
            self.env = os.environ["ENV"]

        config_file = os.path.dirname(__file__) + ('/configs/blackpanther-%s.json'
                                                   % self.env)
        if os.path.isfile(config_file):
            with open(config_file) as f:
                self.json = json.loads(f.read())

        self.from_environment_variables()

    def from_environment_variables(self):
        """ Updates the config from envrionment variables """
        for k, v in iteritems(self.json):
            if k in os.environ:
                logger.warn('change value of config %s from %s to %s'
                            % (k, v, os.environ[k]))
                self.json[k] = os.environ[k]

    def __repr__(self):
        return str(self.json)

    def __getattr__(self, attr):
        if attr in self.json:
            return self.json[attr]

        return None


global_config = Configuration()


if __name__ == '__main__':
    print(global_config)
