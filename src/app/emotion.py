import requests
import json


class EmotionDetection(object):
    def __init__(self, sub_key, api_url, img_url):
        self.return_emotion = ''
        self.return_occlusion = ''
        self.head_pose = ''
        self.sub_key = sub_key
        self.api_url = api_url
        self.img_url = img_url
        self.people_count = 0
        self.response = []
        self.headers = {
            'Ocp-Apim-Subscription-Key': self.sub_key
        }
        self.params = {
            'returnFaceId': 'true',
            'returnFaceLandmarks': 'false',
            'returnFaceAttributes': 'headPose,emotion,occlusion',
        }
    
    def response2server(self):
        self.response = requests.post(
            self.api_url,
            params = self.params,
            headers = self.headers,
            json = {
                "url": self.img_url
            }
        )
        return self.response
    
    def decision_criteria(self):
        count = len(self.response2server().json())
        try: 
            if count != 0:
                self.response = self.response.json()[0]
            return self.response, count
        except KeyError:
            return self.response, count
    
    @staticmethod
    def emotion_selection(emotions_dict):
        result = ''
        value = -1
        for emotion in emotions_dict:
            if emotions_dict[emotion] > value:
                value = emotions_dict[emotion]
                result = emotion
        return result

    def get_result(self):
        self.response, self.people_count = self.decision_criteria()
        if self.people_count == 0: 
            return None, None, None
        else:
            try:
                self.return_emotion = self.emotion_selection(
                    self.response['faceAttributes']['emotion'])
                self.return_occlusion = self.response['faceAttributes']['occlusion']
                self.head_pose = self.response['faceAttributes']['headPose']
                return self.return_emotion, self.return_occlusion, self.head_pose
            except TypeError:
                return None, None, None
    