from flask import Flask, Response, jsonify, request
from flask import Blueprint
from flask_restplus import Api, Resource
from flask_cors import CORS
import app.config as config


config = config.global_config
app = Flask(__name__)
CORS(app)

# Flask RestPlus initializers.
blueprint = Blueprint("api", 
    __name__, url_prefix="/api")

api = Api(blueprint,
          version="0.1",
          title="eFunLearning BAckend System",
          doc="/doc/",
          validate=True)

@api.route('/actions/reset')
class Reset(Resource):
    @api.response(200, "Success")
    @api.response(400, "Error")
    def get(self):
        print("reset the actions")
        return 'Successfully reset'

@api.route('/actions/pause')
class Reset(Resource):
    @api.response(200, "Success")
    @api.response(400, "Error")
    def get(self):
        print("pause the actions")
        return 'Successfully pause'

@api.route('/actions/restart')
class Reset(Resource):
    @api.response(200, "Success")
    @api.response(400, "Error")
    def get(self):
        print("restart the actions")
        return 'Successfully restart'

@api.route('/get_file/csv_file')
class Reset(Resource):
    @api.response(200, "Success")
    @api.response(400, "Error")
    def get(self):
        print("restart the actions")
        return 'Successfully restart'

def errorHandler():
    @app.errorhandler(400)
    def bad_request(error):
        return jsonify({
            'error': 'Bad Request',
        }), 400  # Return HTTP error code 400

# Handle HTTP error code 401
@app.errorhandler(401)
def unauthorized(error):
    return jsonify({
        'error': 'Unauthorized',
    }), 401  # Return HTTP error code 401

# Handle HTTP error code 403
@app.errorhandler(403)
def forbidden(error):
    return jsonify({
        'error': 'Forbidden',
    }), 403  # Return HTTP error code 403

# Handle HTTP error code 404
@app.errorhandler(404)
def not_found(error):
    return jsonify({
        'error': 'Not Found',
    }), 404  # Return HTTP error code 404

# Handle HTTP error code 405
@app.errorhandler(405)
def method_not_allowed(error):
    return jsonify({
        'error': 'Method Not Allowed',
    }), 405  # Return HTTP error code 405

# Handle HTTP error code 406
@app.errorhandler(406)
def not_acceptable(error):
    return jsonify({
        'error': 'Not Acceptable',
    }), 406  # Return HTTP error code 406

@app.errorhandler(408)
def timed_out(error):
    return jsonify({
        'error': 'Request Timeout',
    }), 408  # Return HTTP error code 408