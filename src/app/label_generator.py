from tempfile import NamedTemporaryFile
import shutil
import csv
import os
import logging

PROTOCOL = 'tcp'
PORT_NUM = '5555'
FORMAT = '%(asctime)-15s %(port)s %(protocol)-8s %(message)s'
logging.basicConfig(format=FORMAT)
d = {'port': PORT_NUM, 'protocol': PROTOCOL}
logger = logging.getLogger('Labeler')
logger.setLevel(logging.INFO)


class Labeler(object):
    def __init__(self):
        self.filename = './efunLearninig_result.csv'
        self.fields = ['file Title', 'et', 'emo', 'occu', 'hp', 'concentration']
        self.checkFileExistByException(self.filename)
        self.tempfile = NamedTemporaryFile(delete=False)
        # reset the csv file
        self.cleanCSV(self.filename)
        self.concentration = False
        self.tempArr = []
    
    @staticmethod
    def checkFileExistByException(file_path):
        ret = True
        try:
            # Open file object.
            file_object = open(file_path, 'r')
            logger.info('Labeler: File Detected[%s]', file_path, extra=d)
        except FileNotFoundError:
            ret = False
            logger.warn('Labeler: File Not Exist.', extra=d)
            file_object = open(file_path, 'w')
            logger.info('Labeler: File Created[%s]', file_path, extra=d)
        except IOError:
            ret = False
            logger.error('Labeler: %s can\'t be read', file_path, extra=d)
        except PermissionError:
            ret = False
            logger.error('Labeler: %s have no permission', file_path, extra=d)
        return ret
    

    @staticmethod
    def cleanCSV(file_name):
        tmp = open(file_name, "w")
        tmp.truncate()
        logger.info('Labeler: File Initialized[%s]', file_name, extra=d)
    
    def concen_critera(self, pkg):
        if (pkg['et'] == 'center') and (pkg['emo'] == 'neutral'):
            self.concentration = True
        return self.concentration

    def get_outcome(self, pkg, arrayname):
        incoming = [
            arrayname, pkg['et'], pkg['emo'],
            pkg['occu'], pkg['hp'], self.concen_critera(pkg)
        ]
        self.concentration = False
        self.tempArr.append(incoming)
        # create updated version of file
        with open(self.filename, mode='w') as outfile:
            writer = csv.writer(outfile)
            writer.writerow(self.fields)
            writer.writerows(self.tempArr)
            



