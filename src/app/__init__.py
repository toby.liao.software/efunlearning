""" Initialize app. """
from flask import Flask, current_app
from flask_cors import CORS
from app.router import blueprint
from app.router import errorHandler

# Flask config
app = Flask(__name__)
app.register_blueprint(blueprint)
CORS(app)
errorHandler()

