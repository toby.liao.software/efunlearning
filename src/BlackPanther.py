""" Provides entry point of app for flask command in terminal. """

__author__ = "Yu-Cheng Liao (Toby)"
__copyright__ = ("Copyright 2019, eFun Learning Inc."
                 " All rights reserved.")
__credits__ = ["Yu-Cheng Liao (Toby)"]
__version__ = "0.1"
__license__ = "All rights reserved."
__maintainer__ = "Yu-Cheng Liao (Toby)"
__email__ = "yliaoas@connect.ust.hk"
__status__ = "Development"


from app import app  # noqa

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
