# For the application, please start the following program for the service

## structure of the code
![loading](./blackpanther.jpg)

## Installation

(1) the ZMQ server: 

```python
python3 zmq_controller.py
```

(2) poller for image existence detection

```python
python3 container_poller.py
```

(3) (optional) For the demo use. To mock image uploading

```python
python3 api_stream_frame.py
```

## Deployment

For all the service is packed into the docker, for simply deployment.

(1) Build up the docker image
```shell
docker build --rm -t blackpanther/latest:latest .
```
(2) Run the docker image
```shell
docker run --rm -it blackpanther/latest:latest
```

## License

[MIT](https://choosealicense.com/licenses/mit/)

